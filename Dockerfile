FROM registry.gitlab.com/pages/hugo:0.113.0 AS build

RUN apk add --update --no-cache \
    curl \
    curl-dev \
    git \
    go \
    make \
    wget

COPY . /site
WORKDIR /site

RUN make

FROM docker.io/p3terx/darkhttpd:1.14

WORKDIR /public

COPY --from=build /site/public /public

EXPOSE 80

CMD ["/public"]
