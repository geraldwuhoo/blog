.PHONY: clean build

build: static/katex themes/xmin/.git
	hugo --minify --enableGitInfo

themes/xmin/.git:
	git submodule update --init --recursive

static/katex:
	curl -s https://api.github.com/repos/KaTeX/KaTeX/releases/latest \
		| grep "browser_download.*tar.gz" \
		| cut -d: -f2,3 \
		| tr -d \" \
		| wget -i - -O katex.tar.gz
	tar xvzf katex.tar.gz --directory=static
	rm -f katex.tar.gz

clean:
	rm -rf katex.tar.gz static/katex public
