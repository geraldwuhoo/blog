---
title: "About"
date: 2021-08-09T17:29:12-00:00
---

A place for me to document my findings and ramble about idle thoughts.

[![Shimakaze (Kantai Collection)](/images/shimakaze.webp)](https://twitter.com/teliemu190/status/1249019407751208960)
