---
title: "Putting lipstick on a pig: The best (non-self-hosted) email setup"
date: 2021-08-10T21:08:46-00:00
draft: false
tags:
    - encryption
    - privacy
    - security
    - email
categories:
    - technical
---

~~Hot~~ Lukewarm off the heels of my last post &mdash; [Email encryption: The greatest security/privacy theater, maybe ever](/posts/email-encryption-the-greatest-security-privacy-theater/) &mdash; is a post about about my new email setup. As a reminder, **email is an inherently flawed protocol**. Read the aforementioned post for more details.

With that in mind, I will provide an overview of the best email setup I have found to date. It attempts to mitigate as many flaws as possible, but it's impossible to solve all of them.

# What to look for in a mail provider

For various reasons (see: **deliverability**, high-availability, data integrity/retention), I do not host my own email. It is the only major service type that I still offload to a third-party.

I have been on the hunt for an affordable email service provider that grants me similar security/privacy/featureset to hosting my own service. I look for the following features in a mail service:

1. Email encryption[*](/posts/email-encryption-the-greatest-security-privacy-theater/)
1. Proper IMAP/SMTP gateway so I can use my own mail clients
1. Custom domain support
1. Catch-all alias support
1. Generous max alias count at the shared domain
1. Easy way to generate new aliases on mobile
1. Relatively affordable

In short, I have been looking for a unicorn. After a year of searching, I believe I have finally found it.

# mailbox.org, or why I left ProtonMail

> DISCLAIMER: I am not affiliated with or sponsored by mailbox.org in any way. I just like their service.

## Pricing

I was a ProtonMail Professional subscriber for the past year. This is an **€8.00** per month subscription ($9.50 at the time of writing). That's expensive for a single email subscription. For comparison, at the time of writing, a Netflix basic subscription is $8.99/mo and an Amazon Prime subscription is $119/yr ($9.91/mo). While ProtonMail is cool, I don't think it has technical complexity equivalent to Netflix and Amazon Prime.

mailbox.org raised their prices recently if you wish to bring a custom domain, but it's still only €3.00/mo. This is a much more reasonable price than ProtonMail.

## ~~BYOB~~ BYOK: Bring your own key

ProtonMail encrypts your email at rest with PGP encryption. However, ProtonMail generates this keypair for you, and encrypts the keypair with your account password. They technically do not have access to your decrypted private key, but its encrypted form is *still stored on their servers*.

Why take this risk at all? mailbox.org [lets you bring your own PGP keypair](https://kb.mailbox.org/display/MBOKBEN/The+Encrypted+Mailbox). You only give them *the public key* to encrypt your mailbox. There's no need to trust the mail provider to store an encrypted form of your private key.

## An actual IMAP/SMTP gateway

This was one of my biggest gripes with ProtonMail. I prefer to use my own mail clients (Thunderbird, mutt, FairEmail, the list goes on) instead of webmail.

Email clients can connect to a mailserver through a variety of protocols, but for the simplicity of this post, we will assume that it needs both an IMAP port and an SMTP port. IMAP is used for pulling (receiving) mail, and SMTP is used for pushing (sending) mail.

ProtonMail does not support IMAP and SMTP [unless you use their bridge application as a proxy](https://protonmail.com/support/knowledge-base/imap-smtp-and-pop3-setup/). This is nowhere near as smooth as pointing your mail client to actual IMAP and SMTP ports provided by the mail provider. Additionally, since it is a desktop bridge, it rules out any mobile clients except for the official ProtonMail app.

On the other hand, mailbox.org does provide standard IMAP and SMTP access, while still supporting "zero access" PGP encryption for incoming mail. Of the major privacy-focused email providers, Posteo and mailbox.org are the only two that fit both these criteria. However, [Posteo does not allow you to bring your own custom domain](https://posteo.de/en/site/faq). This makes Posteo a non-starter for my use case.

## The rest

Everything else about mailbox.org is about the same as ProtonMail. It supports custom domains, catch-all aliases, and a generous max alias account at the shared domain (`<alias>@mailbox.org`). For this reason, I feel that mailbox.org is strictly a better choice than ProtonMail.

# Email masking service

This is the secret sauce to making emails (relatively) private and secure. Email masking services like [AnonAddy](https://anonaddy.com/) and [SimpleLogin](https://simplelogin.io/) allow you to create random aliases that all forward to your main inbox.

For example, if I had an AnonAddy account, I could randomly generate an alias, 9f29f550@anonaddy.me, that forwards to my main email. I would then use this email, 9f29f550@anonaddy.me, to sign up for a service that requires email. To the service, *all randomly generated `<alias>@anonaddy.me` emails are indistinguishable*.

I use new AnonAddy aliases to sign up for each service. For example, I may have 9f6f8b4e@anonaddy.me signed up for Amazon, and 836fc174@anonaddy.me signed up for PayPal, both forwarded to my main email. How could a data aggregator tell those are the same person? There may be many randomly generated aliases for every service. I'm mixed in with every other AnonAddy user.

Compare this to typical email usage, in which one signs up for many services with the same email. This makes it trivial to tell that bob_jones@gmail.com is the same person signed up for Paypal, Venmo, Amazon, etc.

## Pricing

Both AnonAddy and SimpleLogin have a free tier. However, I pay for a premium plan as I use more aliases than provided in the free tier, and I also want to support development of these products. They are very user-friendly and have high impact towards privacy/anonymity.

At the time of writing, AnonAddy has a Lite plan with reduced functionality for $12/yr. This is a spectacular deal and I recommend that everybody get it if they need more than 20 email aliases.

## Encryption

Cool feature. Both AnonAddy and SimpleLogin allow a user to provide their public PGP key so they can encrypt the email *before* it gets to your main email provider.

For me, this means that any incoming emails to my AnonAddy aliases are encrypted *before* they reach mailbox.org. *mailbox.org never sees the plaintext of the email*. Of course, this isn't a panacea, as email is a [fundamentally flawed protocol](/posts/email-encryption-the-greatest-security-privacy-theater/). However, it can only be a strict benefit to include AnonAddy in your email chain.

While it is possible that AnonAddy could just as likely log your emails before forwarding them, this is unlikely from an economic point of view. The amount they charge for their featureset ($12/yr for the lite plan, which includes 50 aliases and PGP encryption) is far lower than any equivalent full-fledged email provider. They most likely do not have the economic resources to actually store the emails like a full-fledged email provider.

With how easy it is to use mail masking services compared to how high impact they are (removing a persistent unique identifier), there's really no reason not to use one.

# TL;DR

Use an email masking service ([AnonAddy](https://anonaddy.com/), [SimpleLogin](https://simplelogin.io/)) for most signups that do not require PII. Use [mailbox.org](https://mailbox.org) with a custom domain for everything else. This is the best you can do to mitigate a broken standard.

