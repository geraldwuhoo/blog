---
title: "From Docker to Podman... back to Docker"
date: 2022-03-15T10:59:36-07:00
draft: true
tags:
    - IT
    - devops
    - server
categories:
    - technical
---

Docker is the hot thing nowadays. Well, at this point it's lukewarm, but it's only not so "hot" anymore because it's become so ubiquitous in the industry. Having a repeatable and immutable application container simplifies the development + deployment pipeline immensely. No more setting up fragile development environments, no more pet deployments, no more "it works on my machine". But how much do you actually know about the container ecosystem?

# What are containers?

Before continuing, it's important to establish the relationship between Docker and containers in general. Containers are a long-existing concept in computing, similar to virtual machines (VMs). However, while VMs virtualize an entire OS, containers share the host's kernel and only virtualize (containerize) the userspace of a system.

![VMs vs. containers](/images/e7e21312039def7fb50f81f591287ac1.webp)  
[Source](https://cyrille.giquello.fr/informatique/docker)

Similar to how VMs can be run in a variety of ways by a variety of hypervisors, containers can be implemented in different ways for different purposes as well. One popular example is [Linux Containers (LXC)](https://linuxcontainers.org/). LXC acts like an "operating system" container. That is, a user that spins up an LXC container can treat it simply as a lightweight VM that happens to share the host kernel &mdash; it's accessible, persistent, and mutable, just like a VM. To an unsuspecting user of the LXC container, this may be indistinguishable from a VM.

Docker, on the other hand, acts more like an "application" container. To run a Docker container, a user first writes out a series of steps out in a `Dockerfile`. These steps are interpreted to build a Docker container image. Then, this Docker container image can be run to actually run the application. In contrast with the LXC "operating system" container, this container is inherently ephemeral and immutable. All changes made to the container at runtime are lost when the container is removed. If a user wishes to change some properties about a container, they must update the `Dockerfile` and rebuild the container.

Most people nowadays have converged upon the latter for container usage &mdash; ephemeral, immutable containers can be [treated like cattle](https://www.hava.io/blog/cattle-vs-pets-devops-explained). Ephemeral containers means that you can simply destroy and recreate containers at will, and immutable containers means that a new deployment is guaranteed to not have configuration drift.

# Open Container Initiative (OCI)

The minimal overhead and ease of development/deployment provided by Docker took the world by storm. So much so that "Docker" and "containers" have become practically synonymous terms. However, in addition to the different types of containers mentioned above, there are actually other "Docker-like" container implementations.

Docker, Inc. realized early-on that they had a solid formula, and worked with the Linux Foundation in 2015 to establish the [Open Container Intiative](https://opencontainers.org/). OCI is a set of specifications that govern consistent properties of containers.

## [Runtime](https://github.com/opencontainers/runtime-spec)  

Any container runtime that properly implements the OCI runtime spec is indistinguishable at runtime from any other OCI runtime. This means that if a container were to be run by Podman, and then the same container on the same machine were to be run by Docker, they would be *indistinguishable* while running. They will spawn up the same namespaces and processes to run the container, despite being started by different OCI runtimes.

The rabbit hole for this goes far deeper once you start including things like the Container Runtime Interface (CRI) for Kubernetes, so we can simply leave it as "all OCI runtimes run the same way".

## [Image](https://github.com/opencontainers/image-spec)  

All container images following the OCI spec are intercompatible. If you build a "Docker" image, this image is portable and can be run by any other OCI implementation, such as Podman. There is not any concept of a "Docker" image vs. a "Podman" image &mdash; there are just OCI images that they can all run. This is huge for adoption of alternative OCI runtimes.

## [Distribution](https://github.com/opencontainers/distribution-spec)  

The OCI distribution specification standardizes distribution of built images. For example, [DockerHub](https://hub.docker.com/) holds OCI images that can be pulled by any OCI runtime. While seemingly minor in comparison to the others, this is actually quite important. Being able to *run* the same images doesn't necessarily mean you can *pull* and run the same image from the same remote repository. This specification greatly eases the transition between OCI implemntations.

These specifications are strict in their writing, but as you can see, guarantee a huge degree of compatibility. Combining these three specifications, one can build a container with any OCI implementation, run it with any other OCI implementation, and push/pull from repositories with any OCI implementation, seamlessly.

By abstracting away the underlying specifications, developers and operations folks don't necessarily have to agree on the exact runtimes -- a developer may use Docker locally, but the operations team may decide to go with [CRI-O](https://cri-o.io/) to deploy to production.

# Okay, but what's wrong with Docker?

While the open specification is really nice in theory, why would we actually want to move off of Docker in practice?

Docker was the first player on the scene, and while that gave it the first-mover advantage, this also meant that Docker was going to be perpetually "first-gen". There are a few major pain points of Docker.

## Requires a daemon  

Docker requires a constantly running daemon to function. While this may not seem bad at first, this causes significant issues when trying to run docker-in-docker (DinD) jobs. This typically occurs in CI pipelines, which may have workers running in containers themselves. The inner Docker runtime needs its own daemon. This is normally solved by passing the host's daemon socket into the DinD container, which is a huge security implication, as [access to the Docker socket grants root access to the host](https://stackoverflow.com/questions/40844197/what-is-the-docker-security-risk-of-var-run-docker-sock)!

Alternative daemonless OCI implementations don't run into this limitation, such as [Kaniko](https://github.com/GoogleContainerTools/kaniko) to build container images inside container images on Kubernetes.

## Requires root (until recently)

Until recently, Docker had to be run and accessed as the root user. This is a significant security risk, as any escape from the container sandbox would imply root privileges on the host machine.

In addition to security implications, this poses inconvenience for developers and operations. Requiring root simply to build and run images for local development is inconvenient, as one may need to constantly `sudo` to work with their local containers. This can be "solved" by adding the user to the `docker` group, but the [`docker` group is root equivalent](https://github.com/moby/moby/issues/9976)! And we're back to security implications.

## Monolithic application

Docker was created as one big monolithic application (partially the reason why it still requires a daemon). The UNIX philosophy of "do one thing and do it well" is certainly broken here &mdash; Docker is one application that runs a daemon, comes with a container runtime, and comes with the ability to build images. It makes much more sense to have a dedicated application to interface with the OCI runtime, and a dedicated application to build OCI images.

# Enter Podman

With such compatibility between OCI implementations, the landscape seems ripe for alternatives to Docker to come out, while still being able to access all the benefits of the existing ecosystem.

