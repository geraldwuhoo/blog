---
title: "A new website"
date: 2021-04-24T18:01:28-00:00
draft: false
tags:
    - code
    - webdev
    - bloat
categories:
    - rants
---

I have recently gone through the painstaking trouble of rewriting my homepage at [geraldwu.com](https://geraldwu.com/). I have tried to make it as simple, readable, and lightweight as possible.

# Trimming the bloat

It is now a very simple, text-based website, as opposed to the flashy JavaScript monstrosity it used to be. Where unnecessary bloat used to sprawl, now lives lightweight text. The new website only uses simple HTML and CSS, and does not abuse CPU cycles rendering unnecessary animations, smooth scrolling, fancy title bars, or anything else to wastefully accelerate the heat death of the Universe.

Just take a look at my [old website](https://geraldwu.com/old/). It pulled all sorts of JavaScript libraries for JQuery, bootstrap, MDbootstrap, and external fonts from fontawesome and emoji-css. It had all sorts of buttons, smooth scrolling, and “responsive” design using bootstrap. All to simply display static text that could be easily read off of a résumé PDF. And at what cost? *2.60MB*. A mostly text-only page, was *2.60MB*. This is a prime example of modern web bloat.

What's even worse about the old site, was that it was dynamically generated. Every time somebody hit my website, my web server would do a call to a MariaDB instance, fetch all the data with SQL queries, and process the final result. Only then would it send the actual page back to the client. For what reason did I do this? No really, why did I do this? The content was all static anyway. It simply wasted CPU cycles server-side.

The modern web is bloated with unnecessary JavaScript, external images, and other useless “features” that accomplish nothing except stress the CPU. Computers are powerful nowadays, but CPU cycles and network bandwidth aren't free.

Don't just take my rambling about it. Take a look at this report about trying to browse the modern web on a slow connection: [https://danluu.com/web-bloat/](https://danluu.com/web-bloat/). We are currently in an arms race to make “modern” websites more flashy, responsive, and bloated for no reason other than to prove that we can. There is nothing wrong with a simple, text site to convey static information, and I will no longer support a bloated JavaScript web.

# The results

The new website's HTML and CSS is less than 5KiB transferred over the network. When you include all the images for projects and logos, the entire webpage weighs in at only 54KiB over-the-wire. This is thanks to the magical work of image compression, gzip at the webserver level, and most importantly, no modern webdev bloat.

# A new deployment method

Since the new website is a sort of résumé, I found this to be a perfect target of a homebrew static site generator. I whipped up a quick Python script to generate the website from a [YAML configuration file](https://git.geraldwu.com/gerald/geraldwu.com/-/blob/master/config.yml) and a [Jinja2 template](https://git.geraldwu.com/gerald/geraldwu.com/-/blob/master/website/index.jinja2). To add an entry to any section of the site, I only need to add an entry into my very human-friendly YAML configuration file, and run the Python script to regenerate the static site. Full details and code in the [git repo](https://git.geraldwu.com/gerald/geraldwu.com).

*But wait, there's more!*

It would be annoying to constantly generate and upload the site myself. I would have to remember to run a single `rsync` command to my webserver every time I update the site! Now that's just too much work. So instead, I took advantage of my [self-hosted GitLab](https://git.geraldwu.com/)'s CI/CD features, and wrote a [CI/CD pipeline for my static site generation](https://git.geraldwu.com/gerald/geraldwu.com/-/blob/master/.gitlab-ci.yml). Now, whenever I push an update to my git repo (be that for the config file, new content on the site, or just a general bugfix), it will trigger the GitLab CI/CD to build the site automatically and deploy it to my webserver's root. All without me lifting a finger.

As an added bonus, I can embed automatic pipeline environment variables straight into the webpage at build time. If you take a look at the bottom of the page, you'll see some cool stats about the git commit and pipeline that built this iteration of the webpage:

![geraldwu.com footer](/images/57e061e276b8e75dd6874249d908c018017a52c74dd5b36ae70bd9f46c0138c8.jpg)

# Night mode

Turns out, you can have automatic night mode in your site with only CSS media queries. No JavaScript needed! You could've fooled me with all the bloated JavaScript I've seen across the internet to toggle a site into night mode.

Using the media queries in CSS, [geraldwu.com](https://geraldwu.com/) will read your system's theme (even on mobile!) and will alter the theme of the site to light or dark mode to respect your system's settings.

# Final thoughts

I am a strong proponent of minimal software and reducing modern web bloat. The abuse of bloated JavaScript in modern webdev (and even worse, embedding a browser as a pathetic excuse for a “native” application with Electron and friends) is something I have always hated and stood against. It has irked me for a long while that my website stood as a glowing JavaScript tower built to commemorate my hypocrisy.

I'm relieved I finally had time to scratch my itch and redesign my website from the ground up to remedy this problem.
