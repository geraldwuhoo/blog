---
title: "Email encryption: The greatest security/privacy theater, maybe ever"
date: 2021-06-30T19:01:08-00:00
draft: false
tags:
    - encryption
    - privacy
    - security
    - email
categories:
    - technical
---

[Security theater is the practice of taking security measures that are intended to provide the feeling of improved security while doing little or nothing to achieve it.](https://en.wikipedia.org/wiki/Security_theater)

> *"What? How is encrypted email a security/privacy theater?"*

If you had this thought, then you're not alone. After all, email providers such as [ProtonMail](https://protonmail.com/blog/zero-access-encryption/), [Tutanota](https://tutanota.com/secure-email/), [mailbox.org](https://kb.mailbox.org/display/MBOKBEN/The+Encrypted+Mailbox), and [Posteo](https://posteo.de/en/site/encryption#cryptomailstorage), tout their "zero-access encryption". All the emails you receive are encrypted with a key only *you* know. That's great! Nobody snooping on your emails.

This is misleading.

# Emails still arrive at the inbox unencrypted

The vast majority of emails you receive are not going to be end-to-end encrypted &mdash; that is, they are going to be plain-text emails. When they arrive at the email provider, they are *completely readable*. Sure, the provider will *promise* they won't read it, and they *promise* they will encrypt the incoming email immediately. But that's not how this works. Secure and private technology should be inherently trustless by design, and this is clearly the opposite of that.

This is not just theoretical either. Just a few weeks ago (May 2021), [Tutanota was court-ordered to monitor incoming mail for requested accounts](https://www.cyberscoop.com/court-rules-encrypted-email-tutanota-monitor-messages/). This is not an encryption backdoor. Rather, Tutanota now simply saves a plain-text copy of the email before sending it off to their "zero-access" encryption. That's how easy it is for an "encrypted" email provider to read your emails.

# It takes two to ~~tango~~ email

Don't forget &mdash; an email has both a sender and a recipient. Even if you trust that your email provider will encrypt your email "zero-access", there is still a fundamental problem: the person you're talking to is probably not encrypting their email.

This is a bigger concern than most people realize. The vast majority of mail comes out of a few services: Google (Gmail), Yahoo, Microsoft (Outlook). If your conversation partner is using one of these services, then congratulations. Google/Yahoo/Microsoft can still read all of your emails just fine.

> *"Oh, I just get marketing and sign-up/verification emails. I don't actually use email to communicate"*

That doesn't make you immune either (but good on you for using another form of communication). Just as web content hosting has been outsourced to cloud providers, so too has mail delivery. The vast majority of mail newsletters, verifications, etc. are going through a third-party, such as SendGrid and MailChimp. And these third-parties can read your emails just fine too.

It doesn't take much for a data aggregator to correlate all these emails to you. And there goes the benefit of your "unreadable" encrypted-at-rest email.

> *"My contacts and I all use PGP encryption for emails. This doesn't apply to us."*

Well, you're very lucky that you have family/friends willing to learn PGP. But again, that doesn't make you immune either. Let's talk about the elephant in the room &mdash; metadata.

# Metadata

There is a fundamental flaw with email privacy. The metadata (message header) cannot be encrypted. This is a *huge* deal.

Let's give a hypothetical. I sign up for a new account at *Cool New Service*. I get a verification email from them titled *"Welcome to Cool New Service, Gerald. Please verify your account."* Let's say that this email was somehow end-to-end encrypted (best-case scenario). What can my mail provider find out about me? Well, pretty much everything.

The message header of an email *cannot be encrypted*, because it contains crucial information to email routing. It contains, but is not limited to:

1. From:
1. To:
1. Date:
1. Subject:

So, back to the hypothetical. The mail provider sees the following:

1. From: *email@cool-new-service.com*
1. To: *email@domain.com*
1. Date: *current-date*
1. Subject: *Welcome to Cool New Service, Gerald. Please verify your account.*

Looks like the mail provider can see everything necessary. Even though the message body is encrypted, the metadata leakage in message headers is so substantial that we don't even need to see the message body to know all about it.

This is an *unfixable* flaw of email. These fields in the message headers are mandatory for email to work, yet they give away nearly all the information that a data aggregator would want. ["We kill people based on metadata" -NSA](https://abcnews.go.com/blogs/headlines/2014/05/ex-nsa-chief-we-kill-people-based-on-metadata).

And let's not get into the SMTP *Received:* field, which contains your *IP address*. Not the mail server's IP address, *your* IP address. Just keep this in mind whenever you're sending "private" emails.

## Metadata sidenote: Your connections graph

This is more valuable information than you think. Even if you were to rewrite all your subject details such that your mail provider can only read the To: and From: fields, *this is enough information for data aggregators*.

Think about it. Your connections graph is pretty unique to you. You are connected to *friend1*, *friend2*, *friend3*, *coworker1*, *coworker2*, *coworker3*, and also signed up for services *serviceA*, *serviceB*, *serviceC*. How many other people would have that set of connections? Probably none.

Your connections graph will not only deanonymize you, but also profile you. Get a lot of emails from tech newsletters and services? Interested in tech. Lots of emails from a particular political party? Political views identified. Think about how many of these types of emails you get. How easy it would be for someone to profile all your interests and beliefs, just by reading who you get emails from?

This problem is not limited to emails. Any messaging protocol that has To/From metadata leakage has this problem. Simply knowing who you are talking to is enough information for most data aggregators. How do you think Facebook is profiting off WhatsApp, if [WhatsApp messages are end-to-end encrypted with the Signal Protocol](https://signal.org/blog/whatsapp-complete/)?

Protecting this metadata is hard, but [not impossible](https://signal.org/blog/sealed-sender/).

# Perfect forward secrecy (or lack thereof)

Everything we've talked about so far has been about privacy. But, I also promised security theater.

PGP-encrypted email does not have [forward secrecy](https://en.wikipedia.org/wiki/Forward_secrecy). Since email providers use your one public key to encrypt all your emails, then if the corresponding private key is compromised, then *every one of your emails is compromised*.

This is a significant issue. Nearly all end-to-end encrypted messaging platforms (Signal, Matrix, Threema, Briar, even WhatsApp) implement some form of session keys such that a future compromise will not also compromise past events.

The usage of asymmetric encryption via public/private key cryptography in long-term storage is also problematic due to an inevitable quantum computing future, but that's an entirely different topic for a different post (*hint hint*).

# Closing thoughts

Encrypting your email is better than not encrypting it at all. I will never recommend against it. But, you should keep a realistic expectation of how private you really are, even with the best email practices.

At the end of the day, all of these shortcomings are no fault of the email providers. They're trying their best with what they have, but email is a fundamentally flawed protocol in terms of security/privacy. The ultimate conclusion &mdash; if you need to communicate with someone privately, *don't use email*. Period.
